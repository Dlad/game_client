#ifndef SEARCH_H
#define SEARCH_H

#include <QWidget>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLineEdit>

class Search : public QWidget
{
    Q_OBJECT
public:
    explicit Search(QWidget *parent = 0);

private:
    QHBoxLayout *mainLayout;
    QLineEdit *input;
    QPushButton *search;
signals:

public slots:

};

#endif // SEARCH_H
