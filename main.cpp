#include <QtGui/QApplication>
#include <QTextCodec>

#include "mainwindow.h"
#include "gameshed.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    //设置编码
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(codec);
    QTextCodec::setCodecForCStrings(codec);
    QTextCodec::setCodecForTr(codec);

    MainWindow w;
    //设置标题
    w.setWindowTitle(QObject::tr(VERSION));
    w.show();

    return a.exec();
}
