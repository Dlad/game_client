#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui>
#include <QLabel>
#include <QBoxLayout>
#include <QPushButton>
#include <QtWebKit/QWebView>
#include <QListWidget>

#include "const.h"

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QWebView *view;

public slots:
    void load(QUrl);
};

#endif // MAINWINDOW_H
