#include "search.h"

Search::Search(QWidget *parent) :
    QWidget(parent)
{
    input = new QLineEdit;
    input->setFixedWidth(167);
    input->setObjectName("keyWords");

//    QPixmap *pixmap = NULL;
//    pixmap = new QPixmap(28, 27);
//    pixmap->load(":img/btn_search.png");
//    QIcon *icon = new QIcon(*pixmap);
//    search = new QPushButton(*icon, "", this);
    search = new QPushButton;
    search->setFixedSize(28, 27);
    search->setObjectName("doSearch");
    search->setFocusPolicy(Qt::NoFocus);

    mainLayout = new QHBoxLayout;
    mainLayout->addWidget(input);
    mainLayout->addWidget(search);
    mainLayout->setSpacing(0);

    this->setLayout(mainLayout);
    this->setFixedSize(200, 50);
    this->setObjectName("searchWidget");
}
