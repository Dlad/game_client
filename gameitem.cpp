#include "gameitem.h"
#include "gameshed.h"

GameItem::GameItem(const char *url)
{
    this->isCurrent = false;
    this->url = url;
}

void GameItem::loadUrl()
{
    view->load(QUrl(this->url));
}

void GameItem::setCurrent()
{
    GameShed::unHighlight();
    this->setStyleSheet("background-image: url(:/img/bg_left_content_hover.png);");
}

QWebView *GameItem::view;
