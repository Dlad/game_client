#include "gameshed.h"

//todu: dump
#include <typeinfo>

GameShed::GameShed(QWidget *parent)
    : QWidget(parent)
{
    mainLayout = new QVBoxLayout;

    head = new QLabel(this);
    head->setFixedHeight(30);
    head->setProperty("class", "gameShedTitle");
    mainLayout->addWidget(head);

    gameContainer = new QVBoxLayout;
    gameContainer->setSpacing(0);
    mainLayout->addLayout(gameContainer);

    mainLayout->setContentsMargins(0,0,0,0);
    mainLayout->setSpacing(0);
    mainLayout->addStretch();

    this->setLayout(mainLayout);
    this->setFixedWidth(200);
    this->setMinimumHeight(300);
}

GameItem* GameShed::gameIn(const char *name, const char *url)
{
    GameItem *game = new GameItem(url);

    game->setText(name);
    game->setFixedSize(200, 30);
    game->setFocusPolicy(Qt::NoFocus);
    game->setProperty("class", "gameName");

    this->gameContainer->addWidget(game);
    GameShed::gameList.push_back(game);

    return game;
}

void GameShed::setHeadText(const char *name)
{
    this->head->setText(name);
}

void GameShed::unHighlight()
{
    for(int i=0; i<GameShed::gameList.size(); i++) {
        GameShed::gameList[i]->setStyleSheet("QPushButton.gameName {background-image: url(:/img/bg_left_content.png); background-repeat:repeat-x; height:25px;margin:0;padding:0 0 0 15px;text-align:left;border:none;} QPushButton.gameName:hover {background-image: url(:/img/bg_left_content_hover.png);}");
        //todo:dump
        //qWarning(typeid(GameShed::gameList[i]).name());
    }
}

vector<GameItem*> GameShed::gameList;
