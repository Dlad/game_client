#ifndef GAMESHED_H
#define GAMESHED_H

#include <QtGui>
#include <QBoxLayout>
#include <QPushButton>
#include <QUrl>
#include <QLabel>
#include "gameitem.h"

#include <vector>

using namespace std;
class GameShed : public QWidget
{
    Q_OBJECT
public:
    GameShed(QWidget *parent = 0);
    GameItem* gameIn(const char *, const char *);
    void setHeadText(const char *);
    static vector<GameItem*> gameList;
    static void unHighlight();
private:
    QVBoxLayout *mainLayout;
    QVBoxLayout *gameContainer;
    QLabel *head;
};

#endif // GAMESHED_H
