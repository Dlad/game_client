#include "mainwindow.h"
#include "gameshed.h"
#include "gameitem.h"
#include "search.h"

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
{
    //webkit
    view = new QWebView(this);
    view->page()->setLinkDelegationPolicy(QWebPage::DelegateAllLinks);
    QObject::connect(view, SIGNAL(linkClicked(QUrl)), this, SLOT(load(QUrl)));
    view->load(QUrl("http://www.google.com/"));
    GameItem::view = view;

    //左上
    GameShed *lt = new GameShed(this);
    lt->setHeadText("已安装游戏");

    GameItem *item1 = lt->gameIn("动画片", "http://13hh.com");
    connect(item1, SIGNAL(clicked()), item1, SLOT(loadUrl()));
    connect(item1, SIGNAL(clicked()), item1, SLOT(setCurrent()));

    GameItem *item2 = lt->gameIn("在线杂志", "http://firstmag.net/");
    connect(item2, SIGNAL(clicked()), item2, SLOT(loadUrl()));
    connect(item2, SIGNAL(clicked()), item2, SLOT(setCurrent()));

    //左中搜索框
    Search *lm = new Search(this);

    //左下
    GameShed *lb = new GameShed(this);
    lb->setHeadText("推荐游戏");
    GameItem *item3 = lb->gameIn("最容易", "http://zuirongyi.com");
    connect(item3, SIGNAL(clicked()), item3, SLOT(loadUrl()));
    connect(item3, SIGNAL(clicked()), item3, SLOT(setCurrent()));
    GameItem *item4 = lb->gameIn("团客集", "http://www.tuankeji.com/");
    connect(item4, SIGNAL(clicked()), item4, SLOT(loadUrl()));
    connect(item4, SIGNAL(clicked()), item4, SLOT(setCurrent()));

    //右上
    QPushButton *r = new QPushButton(QPushButton::tr("右"));

    /* 布局开始 */
    QVBoxLayout *mainLayout = new QVBoxLayout;
    //顶部
    QLabel *header = new QLabel(QLabel::tr(VERSION));
    header->setAlignment(Qt::AlignRight);
    header->setStyleSheet("margin-right: 15px;");
    header->setFixedHeight(50);
    mainLayout->addWidget(header);

    //主体
    QHBoxLayout *bottomLayout = new QHBoxLayout;
    //用户操作栏
    QVBoxLayout *leftLayout = new QVBoxLayout;
    leftLayout->addWidget(lt);
    leftLayout->addWidget(lm);
    leftLayout->addWidget(lb);
//    leftLayout->setSpacing(0);
    //展示栏
    QVBoxLayout *rightLayout = new QVBoxLayout;
    rightLayout->addWidget(r);
    rightLayout->addWidget(view);

    bottomLayout->addLayout(leftLayout);
    bottomLayout->addLayout(rightLayout);
    //todo:dump
    bottomLayout->addStretch(0);

    mainLayout->addLayout(bottomLayout);
    /* 布局结束 */

    setLayout(mainLayout);

    QFile qss(":qss/main.qss");
    qss.open(QFile::ReadOnly);
    setStyleSheet(qss.readAll());
    qss.close();
}

MainWindow::~MainWindow()
{

}

void MainWindow::load(QUrl url)
{
    view->load(url);
}
