#ifndef GAMEITEM_H
#define GAMEITEM_H

#include <QPushButton>
#include <QUrl>
#include <QtWebKit/QWebView>

class GameItem : public QPushButton
{
    Q_OBJECT
public:
    GameItem(const char *);
private:
    const char *url;
    bool isCurrent;
public:
    static QWebView *view;

public slots:
    void loadUrl();
    void setCurrent();
};

#endif // GAMEITEM_H
